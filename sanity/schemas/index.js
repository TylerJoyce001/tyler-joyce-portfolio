import blockContent from './blockContent'
import category from './category'
import post from './post'
import author from './skill'

export const schemaTypes = [post, author, category, blockContent]
