import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: '0o6ti6dr',
    dataset: 'production'
  }
})
