import type { JobExperienceType } from "./types/ExperienceCardProps.types";
import CacheHeap from "./assets/cacheHeap.svg";

const jobExperience: JobExperienceType = {
  jobs: [
    {
      positionTitle: "Software Engineering Immersive Resident",
      jobName: "Galvanize Inc.",
      startingDates: "December 2022",
      endingDates: "Present",
      companyLogo:
        "https://pbs.twimg.com/profile_images/1601341524129611776/DJAdDumt_400x400.png",
      summaryPoints: [
        "Mentored over 75 students in creating projects utilizing technologies such as Django, React, Docker, PostgreSQL, external APIs, and CI/CD.",
        "Collaborated with cross-functional teams to ensure the successful implementation of new features and improvements on the student information portal.",
        "Worked directly on the student information portal, implementing new features and improvements for student and staff use.",
        "Utilized agile development methodologies to manage project timelines and deliverables.",
      ],
      languageIcons: {
        javascript: true,
        python: true,
        django: true,
        docker: true,
        mongoDB: true,
        postgresSQL: true,
        react: true,
        nodeJS: true,
      },
    },
    {
      positionTitle: "Full-Stack Software Engineer",
      jobName: "CacheHeap LLC.",
      startingDates: "January 2021",
      endingDates: "Present",
      companyLogo: "/cacheHeap.svg",
      summaryPoints: [
        "Experience utilizing Tailwind for creating user-friendly front-end design",
        "Familiarity with various development tools such as Git, Webpack, and NPM",
        "Strong proficiency in JavaScript, React, Python, and Node.js for full stack development",
        "Strong understanding of software development principles and ability to work in a fast-paced, collaborative environment.",
      ],
      languageIcons: {
        javascript: true,
        python: true,
        react: true,
        nodeJS: true,
        tailwind: true,
      },
    },
  ],
};

export default jobExperience;
