import React from "react";
import { motion } from "framer-motion";

type Props = {};

export default function About({}: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      className="flex flex-col relative h-screen text-center md:text-left md:flex-row
  max-w-7xl px-10 justify-evenly mx-auto items-center"
    >
      <h3 className="absolute top-24 uppercase tracking-[20px] text-gray-500 text-2xl">
        About
      </h3>

      <motion.img
        initial={{
          x: -200,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{ opacity: 1, x: 0 }}
        viewport={{ once: true }}
        src="https://i.ibb.co/FgyTkz8/226721803-152612870313927-7622241965117068205-n.jpg"
        className="-mb-20 md:mb-0 flex-shrink-0 w-56 h-56 rounded-full object-cover md:rounded-lg md:w-64 md:h-96 xl:w-[500px] xl:h-[600px]"
      />

      <div className="space-y-10 px-0 md:px-10">
        <h4 className="text-4xl font-semibold">
          Here is a{" "}
          <span className="underline decoration-[#F7AB0A]/50">little</span>{" "}
          background
        </h4>
        <p className="text-sm justify-center">
          Ever since I was introduced to the world of software developement, I
          was hooked. I knew that I wanted to develop the skills to create
          something that can help make life a little easier. I find it extremely
          rewarding to take a complex set of instructions and boil it down to
          its most essential components. I am passionate about making a
          meaningful contribution to solving some of the world's most pressing
          problems. I am particularly drawn to coding because it allows me to
          create something that can be used and enjoyed by others. I find it
          incredibly satisfying to improve things and make people's lives
          easier. I am always looking for ways to improve my software solutions
          and I am a collaborative, strategically-minded software engineer who
          thinks beyond the code. I focus on the users of the software and their
          experiences, and the impact that development decisions could have on
          the team and company. I have found that I have a natural talent for
          front-end design using JavaScript technologies such as React and React
          Native. I enjoy back-end development as well, but there is something
          particularly gratifying about seeing my UI/UX designs come to life. I
          am always looking to improve my front-end development skills and am
          currently learning and using other software and tools to help me do
          this. Websites such as Airbnb and Smart TV applications like Disney+
          are great inspirations for me. They are beautifully designed and
          intuitive, and I would love to be a part of a team that creates
          something similar. My technical skills include JavaScript, Python,
          TypeScript, NextJS, Django, React.js, Redux, HTML, CSS, Tailwind,
          Bootstrap, Heroku, PostgreSQL, MongoDB, Apache Kafka, AWS, Heroku,
          JSON, Nomad, and Consul. Outside of work, I am a huge foodie and love
          to travel and try new foods. I also enjoy camping and going on cruises
          with my fiancée.
        </p>
      </div>
    </motion.div>
  );
}

// ended 2 hour 24 min mark
