import React from "react";
import { IconsComponent } from "./Icons/IconsComponent";
import { motion } from "framer-motion";
import { ExperienceCardProps } from "../types/ExperienceCardProps.types";

const ExperienceCard: React.FC<ExperienceCardProps> = ({
  positionTitle,
  jobName,
  companyLogo,
  startingDates,
  endingDates,
  summaryPoints,
  languageIcons,
}) => {
  const styles = {
    colors: {
      white: "#000",
    },
  };

  return (
    <article className="experience__card__wrapper">
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{ duration: 1.2 }}
        whileInView={{ opacity: 1, y: 0 }}
        viewport={{ once: true }}
        className="w-32 h-32 rounded-full xl:w-[200px] xl:h-[200px] object-cover object-center"
        src={companyLogo}
        alt={jobName}
      />
      <div className="px-0 md:px-10">
        <h4 className="text-4xl font-light">{positionTitle}</h4>
        <p className="font-bold text-2xl mt-1">{jobName}</p>
        <IconsComponent languageIcons={languageIcons} />
        <div className="flex flex-row items-center justify-start text-base">
          <p className="uppercase py-5 text-gray-300 after:content-['\00a0-\00a0']">
            {startingDates}
          </p>
          <p className="uppercase py-5 text-gray-300">{endingDates}</p>
        </div>
        <ul className="list-disc space-y-4 ml-5 text-lg">
          {summaryPoints.map((point, index) => {
            return <li key={`sp-${index + 1}`}>{point}</li>;
          })}
        </ul>
      </div>
    </article>
  );
};

export default ExperienceCard;
