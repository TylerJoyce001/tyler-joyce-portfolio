import React from "react";
import { motion } from "framer-motion";

type Props = {};

function Projects({}: Props) {
  const projects = [1, 2];

  return (
    <motion.div
      initial={{ opacity: 0 }}
      whileInView={{ opacity: 1 }}
      transition={{ duration: 1.5 }}
      className="h-screen relative flex overflow-hidden flex-col text-left md:flex-row max-w-full justify-evenly mx-auto items-center z-0"
    >
      <h3 className="absolute top-24 uppercase tracking-[20px] text-gray-500 text-2xl">
        Projects
      </h3>
      <div className="relative w-full flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-20 scrollbar-thin scrollbar-track-gray-400/20 scrollbar-thumb-[#F7AB0A]/80">
        {projects.map((project, i) => (
          <div key={i} className="w-screen flex-shrink-0 snap-center flex flex-col space-y-5 items-center justify-center p-10 md:p-44 h-screen">
            <motion.img
              initial={{
                y: -300,
                opacity: 0,
              }}
              transition={{ duration: 1.2 }}
              whileInView={{ opacity: 1, y: 0 }}
              viewport={{ once: true }}
              src="https://i.ibb.co/g4Qqdq0/Camp-Sight-Mock-Up4.png"
              alt="Campsight"
            />
            <div className="space-y-10 p-0 md:px-10">
              <h4 className="text-4xl font-semibold text-center">
                <span className="underline decoration-[#F7AB0A]/50">
                  {" "}
                  Case Study {i + 1} of {projects.length}:
                </span>{" "}
                CampSight
              </h4>
              <p className="text-lg text-center md:text-left sm:text-left">
                CampSight is a camping application that uses a combination of
                technologies to provide a seamless user experience. The backend
                of the application is powered by Python and the FastAPI web
                framework. The application also uses mongoDB for its database,
                which is a NoSQL database. Our parks-api also makes use of the
                NPS National Park Service Data API to allow users to find the
                parks they are looking for. The application also use Galvanize's
                JWTDown for FastAPI module for user authentication. On the
                front-end, the application is built using JavaScript and the
                React framework. This allows for a responsive and interactive
                user interface that makes it easy for users to search and find
                national parks. The application also integrates with the Google
                Maps API, which allows users to view maps of the local area
                around the parks as well as see where parks fall over the United
                States and its territories. In addition to the search and map
                features, the application also allows users to favorite and rate
                the camps they have visited. This allows other users to easily
                see which camps are the most popular and make informed decisions
                about visting certain parks. Overall, CampSight is a powerful
                and user-friendly camping application that makes it easy for
                users to find and explore national parks.
              </p>
            </div>
          </div>
        ))}
      </div>
      <div className="w-full absolute top-[30%] bg-[#F7AB0A]/10 left-0 h-[500px] -skew-y-12"></div>
    </motion.div>
  );
}

export default Projects;
