import React from "react";
import { motion } from "framer-motion";
import { JSIcon } from "./Icons/JSIcon";
import { ReactIcon } from "./Icons/ReactIcon";
import { PythonIcon } from "./Icons/PythonIcon";
import { DjangoIcon } from "./Icons/DjangoIcon";
import { NodeJSIcon } from "./Icons/NodeJSIcon";
import { PostgresSQLIcon } from "./Icons/PostgresSQLIcon";
import { MongoDBIcon } from "./Icons/MongoDBIcon";
import { DockerIcon } from "./Icons/DockerIcon";
import { TailwindIcon } from "./Icons/TailwindIcon";
import { NextJSIcon } from "./Icons/NextJSIcon";
import { TypescriptIcon } from "./Icons/TypescriptIcon";
import { FastAPIIcon } from "./Icons/FastAPIIcon";
import { GitIcon } from "./Icons/GitIcon";
import { SQLLiteIcon } from "./Icons/SQLLiteIcon";
import { HtmlIcon } from "./Icons/HtmlIcon";
import { CssIcon } from "./Icons/CssIcon";

type Props = {
  skill: string;
};

const Skill: React.FC<Props> = (props) => {
  const skill = props.skill;
  return (
    <div className="group relative flex cursor-pointer">
      <motion.div
        initial={{
          x: 200,
          opacity: 0,
        }}
        transition={{ duration: 1 }}
        whileInView={{ opacity: 1, x: 0 }}
        className="rounded-3xl border border-gray-500 object-cover w-24 h-24 md:w-28 md:h-28 xl:w-32 xl:h-32 filter group-hover:grayscale transition duration-300 ease-in-out"
      >
        {skill === "javascript" && <JSIcon style="skill__card" />}
        {skill === "react" && <ReactIcon style="skill__card" />}
        {skill === "python" && <PythonIcon style="skill__card" />}
        {skill === "django" && <DjangoIcon style="skill__card" />}
        {skill === "nodeJS" && <NodeJSIcon style="skill__card" />}
        {skill === "postgresSQL" && <PostgresSQLIcon style="skill__card" />}
        {skill === "mongoDB" && <MongoDBIcon style="skill__card" />}
        {skill === "docker" && <DockerIcon style="skill__card" />}
        {skill === "tailwind" && <TailwindIcon style="skill__card" />}
        {skill === "nextJS" && <NextJSIcon style="skill__card" />}
        {skill === "typescript" && <TypescriptIcon style="skill__card" />}
        {skill === "fastapi" && <FastAPIIcon style="skill__card" />}
        {skill === "git" && <GitIcon style="skill__card" />}
        {skill === "sqllite" && <SQLLiteIcon style="skill__card" />}
        {skill === "html5" && <HtmlIcon style="skill__card" />}
        {skill === "css3" && <CssIcon style="skill__card" />}
      </motion.div>
      <div className="absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-24 w-24 md:w-28 md:h-28 xl:w-32 xl:h-32 rounded-3xl z-0">
        <div className="flex items-center justify-center h-full">
          <p className="text-sm font-bold text-black opacity-100">
            {skill.toUpperCase()}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Skill;
