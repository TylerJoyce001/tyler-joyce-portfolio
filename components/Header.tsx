import React from "react";
import { SocialIcon } from "react-social-icons";
import { motion } from "framer-motion";
import MailOutlineIcon from "@mui/icons-material/MailOutline";

interface Props {}

export default function Header({}: Props) {
  return (
    <header className="sticky top-0 p-5 flex items-start justify-between max-w-7xl mx-auto z-20 xl:items-center">
      <motion.div
        initial={{
          x: -500,
          opacity: 0,
          scale: 0.5,
        }}
        animate={{
          x: 0,
          opacity: 1,
          scale: 1,
        }}
        transition={{
          duration: 1.5,
        }}
        className="flex flex-row items-center"
      >
        <SocialIcon
          url="https://gitlab.com/tylerjoyce001"
          fgColor="grey"
          bgColor="transparent"
        />
        <SocialIcon
          url="https://github.com/tylerjoyce001"
          fgColor="grey"
          bgColor="transparent"
        />
        <SocialIcon
          url="https://www.linkedin.com/in/tylerjoyce001/"
          fgColor="grey"
          bgColor="transparent"
        />
        <SocialIcon
          url="https://instagram.com/_tylerjoyce_"
          fgColor="grey"
          bgColor="transparent"
        />
      </motion.div>
      <a href="#contact">
        <motion.div
          initial={{
            x: 500,
            opacity: 0,
            scale: 0.5,
          }}
          animate={{
            x: 0,
            opacity: 1,
            scale: 1,
          }}
          transition={{
            duration: 1.5,
          }}
          className="flex flex-row items-center text-gray-300 cursor-pointer"
        >
          <MailOutlineIcon className="mr-6 text-[#F7AB0A] h-7 w-7 animate-pulse" />
        </motion.div>
      </a>
    </header>
  );
}
