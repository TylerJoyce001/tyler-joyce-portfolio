import React from "react";
import { JSIcon } from "./JSIcon";
import { ReactIcon } from "./ReactIcon";
import { PythonIcon } from "./PythonIcon";
import { DjangoIcon } from "./DjangoIcon";
import { NodeJSIcon } from "./NodeJSIcon";
import { PostgresSQLIcon } from "./PostgresSQLIcon";
import { MongoDBIcon } from "./MongoDBIcon";
import { DockerIcon } from "./DockerIcon";
import { TailwindIcon } from "./TailwindIcon";

export function IconsComponent(props) {
  const {
    javascript,
    react,
    python,
    django,
    nodeJS,
    postgresSQL,
    mongoDB,
    docker,
    tailwind,
  } = props.languageIcons;
  return (
    <div className="flex flex-row space-x-2 my-2">
      {javascript && <JSIcon />}
      {react && <ReactIcon />}
      {python && <PythonIcon />}
      {django && <DjangoIcon />}
      {nodeJS && <NodeJSIcon />}
      {postgresSQL && <PostgresSQLIcon />}
      {mongoDB && <MongoDBIcon />}
      {docker && <DockerIcon />}
      {tailwind && <TailwindIcon />}
    </div>
  );
}
