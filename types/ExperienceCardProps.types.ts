type IconKey =
  | "javascript"
  | "python"
  | "django"
  | "docker"
  | "mongoDB"
  | "postgresSQL"
  | "react"
  | "nodeJS"
  | "tailwind"
  | "nextJS"
  | "typescript"
  | "fastapi"
  | "git"
  | "sqllite"
  | "html5"
  | "css3";

export interface ExperienceCardProps {
  positionTitle: string;
  jobName: string;
  companyLogo: string;
  startingDates: string;
  endingDates: string;
  summaryPoints: string[];
  languageIcons: Partial<Record<IconKey, boolean>>;
}
export type JobExperienceType = { jobs: ExperienceCardProps[] };
