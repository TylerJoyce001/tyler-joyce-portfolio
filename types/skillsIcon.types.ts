export interface SkillsTypes {
  javascript: boolean;
  python: boolean;
  django: boolean;
  docker: boolean;
  mongoDB: boolean;
  postgresSQL: boolean;
  react: boolean;
  nodeJS: boolean;
  tailwind: boolean;
  nextJS: boolean;
  typescript: boolean;
  fastapi: boolean;
  git: boolean;
  sqllite: boolean;
  html5: boolean;
  css3: boolean;
}
